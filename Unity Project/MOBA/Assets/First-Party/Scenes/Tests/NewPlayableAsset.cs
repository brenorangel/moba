﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[System.Serializable]
public class NewPlayableAsset : PlayableAsset
{
    public int i;

	// Factory method that generates a playable based on this asset
	public override Playable CreatePlayable(PlayableGraph graph, GameObject go)
	{
	    i = 0;
        return Playable.Create(graph);
    }
    
    public void OnEnable()
    {
        Testar();
        i++;
    }

    public void Testar()
    {
        Debug.Log("Teste");
        i++;
    }

    void OnDisable()
    {
        Debug.Log("Fim Teste");
    }
}
