﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbsRotator : MonoBehaviour
{
    public Vector3 Angles;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(Angles);
	}
}
