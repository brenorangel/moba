﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbsPositioner : MonoBehaviour
{
    public bool Rising = false;
    public float MaxDistance = 0.5f;
    public float Speed = 0.01f;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.localPosition.y <= -MaxDistance)
        {
            Rising = transform;
        }
        else if (transform.localPosition.y >= MaxDistance)
        {
            Rising = false;
        }

        if (Rising)
        {
            transform.Translate(0, Speed, 0);
        }
        else
        {
            transform.Translate(0, -Speed, 0);
        }
    }
}

