﻿using UnityEngine;

[System.Serializable]
public struct PrimaryStats
{
    [Range(0, 100)]
    public int Agility;
    [Range(0, 100)]
    public int Vitality;
    [Range(0, 100)]
    public int Strength;
    [Range(0, 100)]
    public int Intelligence;
    [Range(0, 100)]
    public int Spirit;
    [Range(0, 100)]
    public int Dexterity;
    [Range(0, 100)]
    public int Luck;
}
