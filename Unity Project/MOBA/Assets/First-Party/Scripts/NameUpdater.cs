﻿using UnityEngine;

[ExecuteInEditMode]
public class NameUpdater : MonoBehaviour {
    void OnEnable()
    {
        transform.name = $"Player: {GetComponent<Character>().CharacterInfo.Name} {GetComponent<Character>().CharacterInfo.House}";
    }
}
