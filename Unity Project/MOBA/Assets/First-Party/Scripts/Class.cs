﻿using UnityEngine;
using UnityEngine.Timeline;

[System.Serializable]
[CreateAssetMenu(fileName = "new Class", menuName = "Assets/Class")]
public class Class : ScriptableObject
{
    public string Name;
    public SecondaryStats SecondaryStats;

    [Header("Timelines")]
    public TimelineAsset WalkTimeline;        //WASD + Shift or Sticks
    public TimelineAsset RunTimeline;         //WASD or Sticks

    [Header(null)]
    public TimelineAsset InteractionTimeline; //E or Y
    public TimelineAsset JumpTimeline;        //Space or A
    public TimelineAsset AttackTimeline;      //Mouse Click or X
    public TimelineAsset DefenseTimeline;     //Alt Key or B

    [Header(null)]
    public TimelineAsset FirstSkillTimeline;  //1 or LB
    public TimelineAsset SecondSkillTimeline; //2 or LT
    public TimelineAsset ThirdSkillTimeline;  //3 or RB
    public TimelineAsset FourthSkillTimeline; //1 or RT

}