﻿using System.Linq;
using UnityEngine;

public class OldCharacterController : MonoBehaviour
{
    private Animator _animator;
    private string[] _parameters;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        //Resets the boolean type parameters
        _parameters = (from parameter in _animator.parameters
                       let type = parameter.type.ToString().ToLower()
                       where type == "bool"
                       select parameter.name).ToArray();
    }

    private void Update()
    {
        var direction = new Vector3();
        const float deadZone = 0.2f;
        var hAxis = Input.GetAxis("Horizontal");
        var vAxis = Input.GetAxis("Vertical");
        var tmp = new Vector3(hAxis, 0, vAxis);
        if (tmp.sqrMagnitude > deadZone) direction = tmp.normalized;

        var mag = direction.magnitude;
        _animator.SetFloat("Velocity", mag);

        if (mag > 0 && Input.GetKey(KeyCode.LeftShift)) _animator.SetFloat("Velocity", 0.49f);

        //
        if (Input.GetButton("Fire1")) _animator.SetBool("Attack", true);
        if (Input.GetButton("Fire2")) _animator.SetBool("Defend", true);
        if (Input.GetButton("Fire3")) _animator.SetBool("Interact", true);
        if (Input.GetButtonDown("Jump")) _animator.SetBool("Jump", true);

        //Skills
        if (Input.GetKeyDown(KeyCode.Alpha1)) //Or LT
            _animator.SetBool("FirstSkill", true);
        if (Input.GetKeyDown(KeyCode.Alpha2)) //Or LB
            _animator.SetBool("SecondSkill", true);
        if (Input.GetKeyDown(KeyCode.Alpha3)) //Or RT
            _animator.SetBool("ThirdSkill", true);
        if (Input.GetKeyDown(KeyCode.Alpha4)) //Or RB
            _animator.SetBool("FourthSkill", true);
    }

    void LateUpdate()
    {
        //_animator.SetFloat("Velocity", 0);
        foreach (var parameter in _parameters) _animator.SetBool(parameter, false);
    }
}