﻿using UnityEngine;
using UnityEngine.Playables;

[RequireComponent(typeof(Class))]
[RequireComponent(typeof(PlayableDirector))]
public class CharacterController : MonoBehaviour
{
    public PlayableDirector Director;

    // Use this for initialization
    void OnEnable()
    {
        Director = GetComponent<PlayableDirector>();
    }

    private void Update()
    {
        var direction = new Vector3();
        const float deadZone = 0.2f;
        var hAxis = Input.GetAxis("Horizontal");
        var vAxis = Input.GetAxis("Vertical");
        var tmp = new Vector3(hAxis, 0, vAxis);
        if (tmp.sqrMagnitude > deadZone) direction = tmp.normalized;

        var mag = direction.magnitude;

        if (mag > 0)
        {
            Director.Play(GetComponent<Character>().CharacterInfo.Class.WalkTimeline);
            if (Input.GetKey(KeyCode.LeftShift))
            {
            }
        }

        //
        if (Input.GetButton("Fire1")) { }
        if (Input.GetButton("Fire2")) { }
        if (Input.GetButton("Fire3")) { }
        if (Input.GetButtonDown("Jump"))
        {
            Director.Play(GetComponent<Character>().CharacterInfo.Class.JumpTimeline);
        }

        //Skills
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Director.Play(GetComponent<Character>().CharacterInfo.Class.FirstSkillTimeline);
        }//Or LT
        if (Input.GetKeyDown(KeyCode.Alpha2)) { }//Or LB
        if (Input.GetKeyDown(KeyCode.Alpha3)) { }//Or RT
        if (Input.GetKeyDown(KeyCode.Alpha4)) { }//Or RB
    }
}