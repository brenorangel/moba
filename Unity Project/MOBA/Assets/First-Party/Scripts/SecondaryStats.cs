﻿using UnityEngine;

[System.Serializable]
public struct SecondaryStats
{
    public float HealthPoints;
    [SerializeField]
    public enum SpiritTypes
    {
        Mana,
        Rage,
        Energy
    }
    public SpiritTypes SpiritType;
    public float SpiritPoints;
    public float PhysicalAttack;
    public float MagicAttack;
    [Range(0, 100)]
    public float PhysicalResistance;
    [Range(0, 100)]
    public float MagicResistance;
    [Range(0, 100)]
    public float BlockRate;
    [Range(0, 5)]
    public float AttackSpeed;
    [Range(2, 0.25f)]
    public float CastingTime;
    [Range(0, 5)]
    public float MoveSpeed;
    public float AttackRange;
    [Range(0, 100)]
    public float Accuracy;
    [Range(0, 100)]
    public float CriticalRate;
}
