﻿using UnityEngine;

[ExecuteInEditMode]
public class Character : MonoBehaviour
{
    public CharacterInfo CharacterInfo;

    void Update()
    {
        UpdateStats();
    }

    void UpdateStats()
    {
        CharacterInfo.SecondaryStats.HealthPoints = CharacterInfo.Class.SecondaryStats.HealthPoints + CharacterInfo.PrimaryStats.Vitality;
        CharacterInfo.SecondaryStats.SpiritType = CharacterInfo.Class.SecondaryStats.SpiritType;
        CharacterInfo.SecondaryStats.SpiritPoints = CharacterInfo.Class.SecondaryStats.SpiritPoints + CharacterInfo.PrimaryStats.Spirit * 0.6f;
        CharacterInfo.SecondaryStats.PhysicalAttack = CharacterInfo.Class.SecondaryStats.PhysicalAttack + CharacterInfo.PrimaryStats.Strength * 0.8f;
        CharacterInfo.SecondaryStats.MagicAttack = CharacterInfo.Class.SecondaryStats.MagicAttack + CharacterInfo.PrimaryStats.Intelligence * 0.4f;
        //TODO: SecondaryStats.PhysicalResistance
        //TODO:SecondaryStats.MagicResistance
        //TODO:SecondaryStats.BlockRate
        CharacterInfo.SecondaryStats.AttackSpeed = CharacterInfo.Class.SecondaryStats.AttackSpeed + CharacterInfo.PrimaryStats.Agility * 0.01f;
        CharacterInfo.SecondaryStats.CastingTime = CharacterInfo.Class.SecondaryStats.CastingTime - (CharacterInfo.PrimaryStats.Intelligence * 0.0025f);
        CharacterInfo.SecondaryStats.MoveSpeed = CharacterInfo.Class.SecondaryStats.MoveSpeed + CharacterInfo.PrimaryStats.Agility * 0.01f;
        //TODO:SecondaryStats.AttackRange
        CharacterInfo.SecondaryStats.Accuracy = CharacterInfo.Class.SecondaryStats.Accuracy + CharacterInfo.PrimaryStats.Dexterity * 0.25f;
        CharacterInfo.SecondaryStats.CriticalRate = CharacterInfo.Class.SecondaryStats.CriticalRate + CharacterInfo.PrimaryStats.Luck * 0.75f;
    }
}