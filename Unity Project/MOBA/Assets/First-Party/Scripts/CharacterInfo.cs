﻿using UnityEngine;

[System.Serializable]
public struct CharacterInfo
{
    public string Name;
    public string House;
    [SerializeField]
    public Class Class;
    public PrimaryStats PrimaryStats;
    public SecondaryStats SecondaryStats;
}