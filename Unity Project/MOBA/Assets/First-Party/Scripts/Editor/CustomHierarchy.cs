﻿using System.Linq;
using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class CustomHierarchy : MonoBehaviour
{
    private static readonly Vector2 Offset = new Vector2(0, 2);

    static CustomHierarchy()
    {
        EditorApplication.hierarchyWindowItemOnGUI += HandleHierarchyWindowItemOnGui;
    }

    private static void HandleHierarchyWindowItemOnGui(int instanceId, Rect selectionRect)
    {
        var fontColor = Color.black;
        var backgroundColor = new Color(.76f, .76f, .76f);

        var obj = EditorUtility.InstanceIDToObject(instanceId);

        if (obj != null)
        {
            //var prefabType = PrefabUtility.GetPrefabType(obj);
            //if (prefabType == PrefabType.PrefabInstance)
            if (obj.name.StartsWith("Player:"))
            {
                if (Selection.instanceIDs.Contains(instanceId))
                {
                    //fontColor = Color.black;
                    backgroundColor = new Color(0.24f, 0.48f, 0.90f);
                }

                var offsetRect = new Rect(selectionRect.position + Offset, selectionRect.size);

                EditorGUI.DrawRect(selectionRect, backgroundColor);
                EditorGUI.LabelField(offsetRect, obj.name, new GUIStyle
                {
                    //normal = new GUIStyleState { textColor = fontColor },
                    fontStyle = FontStyle.Bold
                });
                //EditorGUI.DrawPreviewTexture(new Rect(selectionRect.width + 6, offsetRect.position.y - 2, 16, 16),EditorGUIUtility.whiteTexture);
            }
        }
    }
}