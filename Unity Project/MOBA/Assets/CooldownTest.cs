﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CooldownTest : MonoBehaviour
{
    public float duration;

    void OnEnable()
    {
        GetComponent<Image>().fillAmount = 1;

        float seconds = 0;

        while (seconds < duration)
        {
            var time = Time.deltaTime;
            Mathf.Floor(time / 60);// = minutes
            seconds = (time % 60);
            GetComponent<Image>().fillAmount -= seconds/duration;
        }

    }

    void Start()
    {
        //InvokeRepeating("UpdateTime", 1f, 1f);  //1s delay, repeat every 1s
    }

    void Update()
    {
        //GetComponent<Image>().fillAmount -= duration / Time.deltaTime;
    }

    void UpdateTime()
    {
        GetComponent<Image>().fillAmount -= 0.1f;
    }
}
